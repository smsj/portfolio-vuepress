---
works_index: true
hero_text: I'm PhD in ecosystem ecology with experimental research testing the effect
  of climate changes on the microbial community and the nutrient cycle. Former member
  of Aquatic Ecology lab of Federal University of Rio de Janeiro and from the Bromeliad
  Working Group of the University of British Columbia, currently I'm researcher and
  developer at <a href="https://citizenscience.gitlab.io/">Lana</a>, centered on free/open
  source technology and citizen science, and co-founder of <a href="https://hackingecology.com/">Hacking
  Ecology</a>, a group of developers and researchers creating rock-solid and open
  source aquatic monitoring systems.
title: Hero

---
<Hero :text="$page.frontmatter.hero_text" />
<WorksList />