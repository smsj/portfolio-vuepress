---
title: Hacking Ecology
date: 2018-08-01T00:00:00.000+00:00
thumbnail: "/upload/dark_he_transparent.png"
year: '2019'
categories:
- Water
- Monitoring System
- open hardware
description: HHacking Ecology

---
# Hacking Ecology

Hacking ecology combine the power of free-open technology and the expertise in aquatic ecology to bring rock-solid and open source systems for water quality monitoring.

Stay updated: [https://hackingecology.com/](https://hackingecology.com/ "Hacking ecology")

![](/upload/dark_he_transparent.png)