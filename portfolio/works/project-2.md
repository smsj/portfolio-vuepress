---
title: " Laia Database"
date: 2018-08-09T18:05:37.000+00:00
thumbnail: "/upload/cover1.png"
year: '2018'
description: laiadb
categories:
- database
- environmental data
- p2p
- encrypted platform

---
# Laia Database

LaiaDB is a P2P and encrypted database for secure environmental data storing. Its architecture assure data the data can only be accessed by trusted groups or individuals with no risk of manipulation and loss.

![](/upload/cover1.png)