---
title: fractaL
date: 2017-02-04T00:00:00.000+00:00
thumbnail: "/upload/logo_green.svg"
year: '2018'
categories:
- python
- evironmental data
- art
- generative design
description: fractaL

---
# fractaL

fractaL is an eco-art project using environmental data to generate sounds and images. A synesthetic approach on environmental data for digital litteracy and ecological evaluation.

Read more about it: [https://ecofractal.gitlab.io/](https://ecofractal.gitlab.io/ "fractaL")

![](/upload/fractaL_1.png)